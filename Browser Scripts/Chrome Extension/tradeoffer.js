// Copyright (c) 2015 backpack.tf. All rights reserved.

function getParameterByName(name) { 
	var regex = new RegExp("[\\?&]" + name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]") + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results ? decodeURIComponent(results[1].replace(/\+/g, " ")) : false;
}

var t = getParameterByName('for_item');

if (t) {
	var item = t.split("_"),
		scr = document.createElement("script");
	scr.type = "text/javascript";
	scr.textContent = `
		g_rgCurrentTradeStatus = {
			"newversion": true,
			"version": 1,
			"me": {
				"assets": [],
				"currency": [],
				"ready": false
			},
			"them": {
				"assets": [{
					"appid": ${JSON.stringify(item[0])},
					"contextid": ${JSON.stringify(item[1])},
					"assetid": ${JSON.stringify(item[2])},
					"amount": 1
				}],
				"currency": [],
				"ready": false
			}
		};
		RefreshTradeStatus(g_rgCurrentTradeStatus, true);
	`;
	document.head.appendChild(scr);
}