// ==UserScript==
// @name        Automatic Steam Trade Offers
// @namespace   steam
// @description This extension allows to pre-select the seller's item automatically when clicking a trade offer link if an item is specified.
// @match       *://steamcommunity.com/tradeoffer/new/*
// @version     1.1
// @grant       none
// @run-at      document-end
// @updateURL   https://bitbucket.org/srabouin/backpack.tf-automatic/raw/master/Browser%20Scripts/Greasemonkey/Automatic_Steam_Trade_Offers.user.js
// @downloadURL https://bitbucket.org/srabouin/backpack.tf-automatic/raw/master/Browser%20Scripts/Greasemonkey/Automatic_Steam_Trade_Offers.user.js
// ==/UserScript==

// Copyright (c) 2015 backpack.tf. All rights reserved.

function getParameterByName(name) { 
	var regex = new RegExp("[\\?&]" + name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]") + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results ? decodeURIComponent(results[1].replace(/\+/g, " ")) : false;
}

var t = getParameterByName('for_item');

if (t) {
	var item = t.split("_"),
		scr = document.createElement("script");
	scr.type = "text/javascript";
	scr.textContent = `
		g_rgCurrentTradeStatus = {
			"newversion": true,
			"version": 1,
			"me": {
				"assets": [],
				"currency": [],
				"ready": false
			},
			"them": {
				"assets": [{
					"appid": ${JSON.stringify(item[0])},
					"contextid": ${JSON.stringify(item[1])},
					"assetid": ${JSON.stringify(item[2])},
					"amount": 1
				}],
				"currency": [],
				"ready": false
			}
		};
		RefreshTradeStatus(g_rgCurrentTradeStatus, true);
	`;
	document.head.appendChild(scr);
}